﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun1_Script : MonoBehaviour
{
    // Start is called before the first frame update
    
    private Transform Bullet_Spawn_Loc;
    public GameObject Projectile;
    public float shotDelay;
    public float lastShot;

    void Start()
    {
        Bullet_Spawn_Loc = GameObject.Find("Bullet_Spawn").transform;
        lastShot = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        
        if(Input.GetMouseButton(0))
        {
            Debug.Log("Button Hold");
            if ((Time.time - lastShot) >= shotDelay)
            {
                lastShot = Time.time;
                Instantiate(Projectile, Bullet_Spawn_Loc.position, this.gameObject.transform.rotation);
            }
        }/*
        else if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Button Hit");
            Instantiate(Projectile, Bullet_Spawn_Loc.position, this.gameObject.transform.rotation);
        }*/
    }   


    
}
