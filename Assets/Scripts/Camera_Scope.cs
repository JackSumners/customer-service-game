﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Scope : MonoBehaviour
{
    public float Min;
    public float Max;
    public bool ZoomIN;
    

    // Start is called before the first frame update
    void Start()
    {
        ZoomIN = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            if (ZoomIN == false)
            {
                this.gameObject.GetComponent<Camera>().fieldOfView = Max;
                ZoomIN = true;
            }
            else
            {
                this.gameObject.GetComponent<Camera>().fieldOfView = Min;
                ZoomIN = false;
            }

        }
    }
}
