﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Script : MonoBehaviour
{
    // Start is called before the first frame update
    public int B_DMG;
    public int speed;
    public float flightTime;
    private float timeStart;
    //private GameObject Bullet_Spawn_Loc;
    
    void Start()
    {
        timeStart = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if((Time.time - timeStart) >= flightTime)
        {
            Debug.Log("Bullet Destroy");
            Destroy(this.gameObject);
        }
        transform.position += transform.up * speed * Time.deltaTime;
    }

    
}
