﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Movement : MonoBehaviour
{
    CharacterController characterController;
    public GameObject FP_Camera;

    public float moveSpeed;
    private float moveSpeedSave;
    public float turnSpeed;
    public float jumpSpeed;
    public float gravity;

    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -60F;
    public float maximumY = 60F;
    float rotationY = 0F;

    private Vector3 moveDirection = Vector3.zero;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        moveSpeedSave = moveSpeed;
    }


    void Update()
    {
        moveSpeed = moveSpeedSave;

        if (axes == RotationAxes.MouseXAndY)
        {
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            FP_Camera.transform.localEulerAngles = new Vector3(-rotationY, 0, 0); // rotates camera on y but not x
            transform.localEulerAngles = new Vector3(0, rotationX, 0); //rotates body on x but not y
            

        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0); //rotates all on x
            //FP_Camera.transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);

        }
        else
        {
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            //transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
            FP_Camera.transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0); //rotates only camera y

        }

        
      
            
            // We are grounded, so recalculate
            // move direction directly from axes
            if (Input.GetAxis("Vertical") > 0)
            {
                if (Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Horizontal") < 0)
                {
                    moveSpeed /= 1.5f;
                    //moveSpeed = Mathf.Sqrt(moveSpeed);
                }
                transform.position += transform.forward * moveSpeed * Time.deltaTime;
            }
            else if (Input.GetAxis("Vertical") < 0)
            {
                if (Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Horizontal") < 0)
                {      
                    moveSpeed /= 1.5f;
                    //moveSpeed = Mathf.Sqrt(moveSpeed);
                }   
                transform.position -= transform.forward * moveSpeed * Time.deltaTime;
            }

            if (Input.GetAxis("Horizontal") > 0)
            {
                transform.position += transform.right * moveSpeed * Time.deltaTime;
            }
            else if (Input.GetAxis("Horizontal") < 0)
            {
                transform.position -= transform.right * moveSpeed * Time.deltaTime;
            }
        
        //moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        // moveDirection *= moveSpeed;
        if (characterController.isGrounded)
        {
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpSpeed;
            }
        }

        // Apply gravity. Gravity is multiplied by deltaTime twice (once here, and once below
        // when the moveDirection is multiplied by deltaTime). This is because gravity should be applied
        // as an acceleration (ms^-2)
        moveDirection.y -= gravity * Time.deltaTime;

        // Move the controller
        characterController.Move(moveDirection * Time.deltaTime);
    }

}



